<?php
include_once "header.php" ;

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true || $_SESSION["role"] !== 'admin'){
    header("location: login.php");
    exit;
}
// Define variables and initialize with empty values
$name = $credit = $code = $time = "";

$name_err = $credit_err = $code_err = $time_err = "";

// Processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Validate name
    if (empty(trim($_POST["name"]))) {
        $name_err = "Please enter a Name.";
    }

    // Validate role
    if (empty(trim($_POST["credit"]))) {
        $credit_err = "Please enter role.";
    }
    // Validate role
    if (empty(trim($_POST["code"]))) {
        $code_err = "Please enter code.";
    }

    // Validate Department
    if (empty(trim($_POST["time"]))) {
        $time_err = "Please enter Name of Department";
    }

    // Check input errors before inserting in database
    if (empty($name_err) && empty($credit) && empty($department_err) && empty($code_err)) {

        // Prepare an insert statement
        $sql = "INSERT INTO courses (course_name, course_code, credit, course_time) VALUES (?, ?, ?, ?)";

        if ($stmt = mysqli_prepare($link, $sql)) {
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "siis", $param_name, $param_code,$param_credit, $param_time);

            // Set parameters
            $param_name = trim($_POST["name"]);
            $param_credit = trim($_POST["credit"]);
            $param_department = trim($_POST["time"]);
            $param_code = trim($_POST["code"]);


            // Attempt to execute the prepared statement
            if (mysqli_stmt_execute($stmt)) {
                // Redirect to login page
                header("location: home.php");
            } else {
                echo "Something went wrong. Please try again later.";
            }
        }

        // Close statement
        mysqli_stmt_close($stmt);
    }

    // Close connection
    mysqli_close($link);
}
?>


    <h2>Course Registration</h2>
    <p>Please fill this form to store course info.</p>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
            <label>Name</label>
            <input type="text" name="name" class="form-control" value="<?php echo $name; ?>">
            <span class="help-block"><?php echo $name_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($code_err)) ? 'has-error' : ''; ?>">
            <label>code</label>
            <input type="text" name="code" class="form-control" value="<?php echo $code; ?>">
            <span class="help-block"><?php echo $code_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($credit_err)) ? 'has-error' : ''; ?>">
            <label>credit</label>
            <input type="text" name="credit" class="form-control" value="<?php echo $credit; ?>">
            <span class="help-block"><?php echo $credit_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($department_err)) ? 'has-error' : ''; ?>">
            <label>Department</label>
            <input type="time" name="time" class="form-control" value="<?php $date = date('h:i a'); echo "$date"; ?>">
            <!--            <input type="time" name="time" class="form-control" value="hh:mm:ss"></div>-->
            <span class="help-block"><?php echo $time_err; ?></span>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Submit">
            <input type="reset" class="btn btn-default" value="Reset">
        </div>

    </form>
<?php include_once "footer.php"?>