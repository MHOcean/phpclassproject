<?php
include "config.php";
    session_start();

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Attendance System</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
          integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="style.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <!-- favicon -->
    <link rel="shortcut icon" href="logo1.png" type="image/x-icon">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
            integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous">
    </script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"
            integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous">
    </script>

</head>

<body>

<div class="wrapper container">
    <!-- Sidebar  -->
    <nav id="sidebar" class="active">
        <div class="sidebar-header">
            <a class="navbar-brand" href="#">
                <img id="logo" alt="Logo" src="logo1.png" width="75%">
            </a>
        </div>

        <ul class="list-unstyled components">
            <?php if( $_SESSION['role'] == 'teacher'){?>
            <li>
                <a href="teacher.php">Teachers Home</a>
            </li>
            <li>
                <a href="mycourses.php">Pick courses</a>
            </li>
            <?php }
            else if($_SESSION['role'] == 'student'){?>
            <li>
                <a href="mycourses.php">Student's Pick Your poisons</a>
            </li>
                <li>
                    <a href="stat.php">Attendance Statistics</a>
                </li>
            <?php }
            else if($_SESSION['role'] == 'admin'){?>
            <li>
                <a href="course.php"> Create Courses</a>
            </li>
                <li>
                    <a href="stat.php">Attendance Statistics</a>
                </li>
            <?php }?>
<!--            <li>-->
<!--                <a href="#"></a>-->
<!--            </li>-->
        </ul>
        <footer>
            <p>&copy;<small> Designed and Developed by</small> Mahmudul Hasan Sagar 2019</p>
        </footer>
    </nav>

    <!-- page navigation -->
    <div id="content" class="active">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-bars"></i>
                </button>
                <div class="navbar-brand ml-2 ">
                    <a href="home.php">Attendance System</a>
                </div>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav ml-auto">
                        <?php if(isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] == true){?>
                            <li class="nav-item">
                                <a class="nav-link" href="logout.php">
                                    <i class="fas fa-user-minus"></i>
                                    <span>Sign Out</span>
                                </a>
                            </li>
                        <?php  }else{?>
                            <li class="nav-item">
                                <a class="nav-link" href="login.php">
                                    <i class="fas fa-sign-in-alt"></i>
                                    <span>Sign in</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="register.php">
                                    <i class="fas fa-user-plus"></i>
                                    <span>Sign up</span>
                                </a>
                            </li>
                        <?php }?>
                    </ul>
                </div>
            </div>
        </nav>

