<?php
include  "header.php";
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

$query =  mysqli_query($link,"select * from courses");
// Define variables and initialize with empty values
$course_id = $nos = "";
$nos_err = "";
if(empty(trim($_POST["nos"]))){
    $nos_err = "Please enter Number of student.";
}

$checkbox1 = $_POST['chk1'];
if($_SERVER["REQUEST_METHOD"] == "POST"){
    for ($i = 0; $i <count($checkbox1);$i++){
        if(!empty($checkbox1)){
            $st_id = $_SESSION["id"];
            if ($_SESSION['role'] == 'student') {
                $sql = "INSERT INTO course_student (student_id, course_id) VALUES ('" . $st_id . "', '" . $checkbox1[$i] . "')";
                if (mysqli_query($link, $sql))
                    header('location:student.php');
                else
                    die (mysqli_error());
            }
            if ($_SESSION['role'] == 'teacher') {
                if(empty($nos_err)) {
                    $nos = $_POST['nos'];
                    $sql = "INSERT INTO course_teacher (teacher_id, course_id, number_of_student) VALUES ('" . $st_id . "', '" . $checkbox1[$i] . "', '" . $nos . "')";
                    if (mysqli_query($link, $sql))
                        header('location:student.php');
                    else
                        die (mysqli_error());
                }
            }
        }
    }
}
?>

    <h2>MY Courses</h2>
    <p>Please choose your courses</p>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <?php while ($rows = mysqli_fetch_assoc($query)){ ?>
            <div class="form-group">
                <label><?php echo $rows['course_name']?></label>
                <input type="checkbox" name="chk1[]" class="form-control" value="<?php echo $rows['id']; ?>">
            </div>
        <?php } if ($_SESSION['role'] == 'teacher'){?>
        <div class="form-group">
            <label>Number Of Student</label>
            <input type="number" name="nos" class="form-control" value="<?php echo $nos; ?>">
        </div>
        <?php }?>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Submit">
        </div>
    </form>
<?php include "footer.php";?>