<?php
    include "header.php";
    session_start();
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true || $_SESSION["role"] !== 'student'){
    header("location: login.php");
    exit;
}
?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="text-center"> Hello, <?php  echo $_SESSION['name']?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h1>Your Subjects are:</h1>
            <ul class="list-group">
                <li class="list-group-item list-action" aria-disabled="true">Courses</li>
                <?php
                $id = $_SESSION['id'];
                $sql = mysqli_query($link, "SELECT courses.id,courses.course_name FROM courses,course_student WHERE courses.id= course_student.course_id AND course_student.student_id ='".$id."'");
                while ($rows = mysqli_fetch_array($sql)){
                    ?> <p class="list-group-item " ><?php echo $rows['course_name']?></p>
                <?php }?>

        </div>
    </div>
</div>
<?php include "footer.php"?>
