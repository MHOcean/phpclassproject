<?php
include_once "header.php";
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true || $_SESSION["role"] !== 'teacher'){
    header("location: login.php");
    exit;
}
?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center"> Hello,<?php echo $_SESSION['name'];?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1>Your Subjects are:</h1>
                <ul class="list-group">
                    <li class="list-group-item disabled" aria-disabled="true">Courses</li>
                    <?php
                    $id = $_SESSION['id'];
                    $sql = mysqli_query($link, "SELECT courses.id,courses.course_name FROM courses,course_teacher WHERE courses.id= course_teacher.course_id AND course_teacher.teacher_id ='".$id."'");
                    while ($rows = mysqli_fetch_array($sql)){
                        ?>
                        <a  href="take.php?id=<?php echo $rows['id'];?>" class="list-group-item " ><?php $_SESSION['cid']= $rows['id'];echo $rows['course_name']?></a>
                    <?php }?>

            </div>
        </div>
    </div>
<?php include "footer.php"?>